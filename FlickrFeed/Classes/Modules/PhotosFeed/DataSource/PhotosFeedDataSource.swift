//
//  PhotosFeedDataSource.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class PhotosFeedDataSource: NSObject {
    
    private var dataSource: [FlickrImage] = []
    fileprivate var manager = RequestManager.shared
    fileprivate var requestCount = Source.count
    fileprivate var page: Int = 1
    fileprivate var isFinished = false
    fileprivate var query: String = ""
    
    var count: Int {
        return dataSource.count
    }
    
    var lastIndexPath: IndexPath {
        return IndexPath(item: count - 1, section: 0)
    }
    
    func add(_ element: FlickrImage) {
        dataSource.append(element)
    }
    
    func add(_ elements: [FlickrImage]) {
        dataSource.append(contentsOf: elements)
    }
    
    func refresh(_ elements: [FlickrImage]) {
        drop()
        dataSource.append(contentsOf: elements)
    }
    
    func drop() {
        dataSource = []
    }
    
    subscript(indexPath: IndexPath) -> FlickrImage {
        return dataSource[indexPath.item]
    }
    
    func search(_ query: String, completion: @escaping (_ newItems: [IndexPath]) -> ()) {
        self.query = query
        getPhotos(query: query, page: 1) { [weak self] (fetchedItems) in
            guard let strongSelf = self else { return }
            let indexes = IndexPath.indexes(with: 0..<fetchedItems, section: 0)
            if fetchedItems < strongSelf.requestCount {
                strongSelf.isFinished = true
            }
            completion(indexes)
        }
    }
    
    func nextPage(completion: @escaping (_ newItems: [IndexPath]) -> ()) {
        guard !isFinished else { return }
        getPhotos(query: query, page: page) { [weak self] (fetchedItems) in
            guard let strongSelf = self else { return }
            let baseOffset = strongSelf.page * strongSelf.requestCount
            let indexes = IndexPath.indexes(with: baseOffset..<baseOffset+fetchedItems, section: 0)
            if fetchedItems < strongSelf.requestCount {
                strongSelf.isFinished = true
            } else {
                strongSelf.page += 1
            }
            completion(indexes)
        }
    }
}

//MARK: - Network methods
fileprivate extension PhotosFeedDataSource {
    func getPhotos(query: String, page: Int, completion: @escaping (_ fetchedItems: Int) -> ()) {
        manager.flickrSearchQuery(query, page: page, count: requestCount) { [weak self] (result, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
                completion(0)
            } else if let data = result {
                self?.add(data)
                completion(data.count)
            }
        }
    }
}

extension PhotosFeedDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.photosFeedCellID, for: indexPath)!
        photoCell.photo = self[indexPath]
        return photoCell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

