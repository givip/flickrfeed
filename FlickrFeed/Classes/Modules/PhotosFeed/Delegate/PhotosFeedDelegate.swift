//
//  PhotosFeedDelegate.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class PhotosFeedDelegate: NSObject {
    var nextPageBlock: ((IndexPath) -> ())?
}

extension PhotosFeedDelegate: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        nextPageBlock?(indexPath)
    }
}

extension PhotosFeedDelegate: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSideLength = collectionView.bounds.width / 3
        return CGSize(width: cellSideLength, height: cellSideLength)
    }
}
