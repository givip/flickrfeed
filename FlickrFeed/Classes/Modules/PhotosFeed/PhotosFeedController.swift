//
//  PhotosFeedController.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class PhotosFeedController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var query: String? {
        didSet{
            getData(with: query)
        }
    }
    
    fileprivate let dataSource = PhotosFeedDataSource()
    fileprivate let delegate = PhotosFeedDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
    }
    
    private func getData(with query: String?) {
        guard let query = query else { return }
        dataSource.search(query) { [weak self] (newItems) in
            self?.collectionView.insertItems(at: newItems)
        }
    }
    
    fileprivate func nextPage() {
        dataSource.nextPage(completion: { [weak self] (newItems) in
            self?.collectionView.insertItems(at: newItems)
        })
    }
    
}

fileprivate extension PhotosFeedController {
    func configureCollectionView() {
        collectionView.dataSource = dataSource
        delegate.nextPageBlock = { [unowned self] indexPath in
            if indexPath == self.dataSource.lastIndexPath {
                self.nextPage()
            }
        }
        collectionView.delegate = delegate
    }
}

extension PhotosFeedController: ViewControllerProtocol {
    static func create(query: String) -> PhotosFeedController {
        let photosController = PhotosFeedController.create()
        photosController.query = query
        photosController.title = "\"\(query)\""
        return photosController
    }
    
    static func storyBoardName() -> String {
        return "Main"
    }
}

