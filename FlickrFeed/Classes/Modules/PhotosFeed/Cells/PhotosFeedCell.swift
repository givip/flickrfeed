//
//  PhotosFeedCell.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import Kingfisher

class PhotosFeedCell: UICollectionViewCell {

    @IBOutlet fileprivate weak var photoImage: UIImageView!
    
    var photo: FlickrImage? {
        didSet {
            if let photo = photo?.imageUrl {
                photoImage.kf.setImage(with: URL(string: photo))
            } else {
                photoImage.image = nil
            }
        }
    }
}

