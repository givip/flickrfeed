//
//  SearchListController.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class SearchListController: UIViewController {

    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    
    fileprivate let dataSource = SearchListDataSource()
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureSearchController()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tableView.reloadData()
    }
}
// MARK: - Search Bar delegate
extension SearchListController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text else { return }
        dataSource.add(query)
        push(viewController: PhotosFeedController.create(query: query))
    }
}

// MARK: - Configuration methods
fileprivate extension SearchListController {
    func configureSearchController() {
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar
    }
    
    func configureTableView() {
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
}

// MARK: - Table view delegate
extension SearchListController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let query = dataSource[indexPath.row]
        push(viewController:  PhotosFeedController.create(query: query))
    }
}

extension SearchListController: ViewControllerProtocol {
    static func storyBoardName() -> String {
        return "Main"
    }
}
