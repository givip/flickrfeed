//
//  SearchListDataSource.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 09.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class SearchListDataSource: NSObject {
    
    private var dataSource: [String] = []
    
    var count: Int {
        return dataSource.count
    }
    
    func add(_ element: String) {
        if !dataSource.contains(element) {
            dataSource.insert(element, at: 0)
        }
    }
    
    subscript(indexPath: IndexPath) -> String {
        return dataSource[indexPath.row]
    }
    
    subscript(index: Int) -> String {
        return dataSource[index]
    }
    
}

extension SearchListDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.searchListCellID)!
        cell.queryLabel.text = self[indexPath]
        return cell
    }
}
