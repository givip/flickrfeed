//
//  SearchListCell.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class SearchListCell: UITableViewCell {
    @IBOutlet weak var queryLabel: UILabel!
}
