//
//  FlickrImage.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrImage: Mappable {
    
    var id: String?
    var owner: String?
    var secret: String?
    var server: String?
    var farm: Int?
    var title: String?
    var isPublic: Bool?
    var isFriend: Bool?
    var isFamily: Bool?
    
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id         <- map["id"]
        owner      <- map["owner"]
        secret     <- map["secret"]
        server     <- map["server"]
        farm       <- map["farm"]
        title      <- map["title"]
        isPublic   <- map["ispublic"]
        isFriend   <- map["isfriend"]
        isFamily   <- map["isfamily"]
    }
    
    var imageUrl: String? {
        guard let farm = farm, let server = server, let id = id, let secret = secret else { return nil }
        return "https://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg"
    }
}
