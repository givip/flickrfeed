//
//  RequestManager.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

typealias ServerResponse = ([FlickrImage]?, Error?) -> Void

class RequestManager {
    
    static let shared = RequestManager()
    private init() {}
    
    func flickrSearchQuery(_ query: String, page: Int, count: Int, completion: @escaping ServerResponse) {
        var params = Parameters.defaultParams
        params["text"] = query
        params["per_page"] = count
        params["page"] = page
        let request = Alamofire.request(URLs.baseURL,
                                        method: .get,
                                        parameters: params,
                                        encoding: URLEncoding.default,
                                        headers: nil)
        request.responseArray(keyPath: "photos.photo") { (response: DataResponse<[FlickrImage]>) in
            completion(response.result.value, response.error)
        }
        request.responseString { (response) in
            debugPrint(response)
        }
    }
}
