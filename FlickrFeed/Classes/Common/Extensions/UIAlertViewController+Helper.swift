//
//  UIAlertViewController+Helper.swift
//  OxfamTracker
//
//  Created by Givi Pataridze on 03.09.16.
//  Copyright © 2016 Givi Pataridze. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func showErrorAlert(message: String?, controller: UIViewController) {
        self.showCustomAlert(title: "Error", message: message, controller: controller)
    }
    
    static func showWarningAlert(message: String?, controller: UIViewController) {
        self.showCustomAlert(title: "Warning", message: message, controller: controller)
    }
    
    static func showCustomAlert(title: String?, message: String?, controller: UIViewController) {
        let defaultAlertAction = UIAlertAction(title: "OK",style: .default, handler: nil)
        let alertError = UIAlertController(title: title, message: message,  preferredStyle: .alert)
        alertError.addAction(defaultAlertAction)
        controller.present(alertError, animated: true, completion: nil)
    }
    
    static func showActionAlert(title: String?, message: String?, inViewController viewController: UIViewController, actionBlock: (() -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertActionOk = UIAlertAction(title: "OK", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
            if let actionBlock = actionBlock {
                actionBlock()
            }
        }
        alert.addAction(alertActionOk)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func showActionAlertYesNo(title: String?, message: String?, inViewController viewController: UIViewController, actionYesBlock: (() -> ())?, actionNoBlock: (() -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertActionYes = UIAlertAction(title: "YES", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
            if let actionYesBlock = actionYesBlock {
                actionYesBlock()
            }
        }
        alert.addAction(alertActionYes)
        
        let alertActionNo = UIAlertAction(title: "NO", style: .cancel) { action in
            alert.dismiss(animated: true, completion: nil)
            if let actionNoBlock = actionNoBlock {
                actionNoBlock()
            }
        }
        alert.addAction(alertActionNo)
        viewController.present(alert, animated: true, completion: nil)
    }
}
