//
//  IndexPath+Helper.swift
//  FlickrFeed
//
//  Created by Givi on 09/02/2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation

extension IndexPath {
    static func indexes(with range: CountableRange<Int>, section: Int) -> [IndexPath] {
        return range.map({ return IndexPath(item: $0, section: section) })
    }
}
