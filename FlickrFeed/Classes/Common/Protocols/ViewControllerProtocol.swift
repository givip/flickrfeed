//
//  ViewControllerProtocol.swift
//  Everest
//
//  Created by Givi Pataridze on 22.09.16.
//  Copyright © 2016 Givi Pataridze. All rights reserved.
//

import UIKit


public protocol ViewControllerProtocol {
    static func storyBoardName() -> String
    func showAlertWithError(error: NSError)
    func showError(message: String?, isWarning: Bool)
    func showAlert(title: String?)
    func showAlert(title: String?, message: String?)
    func showAlert(title: String?, message: String?, actionBlock: (() -> ())?)
}

public extension ViewControllerProtocol where Self: UIViewController {
    
    static func create() -> Self {
        let storyboard = self.storyboard()
        let className = NSStringFromClass(Self.self)
        let finalClassName = className.components(separatedBy: ".").last!
        let viewControllerId = finalClassName + "ID"
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerId)
        return viewController as! Self
    }
    
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: storyBoardName(), bundle: nil)
    }

    func push(viewController: UIViewController) {
        self.push(viewController: viewController, animated: true)
    }
    
    func push(viewController: UIViewController, animated: Bool) {
        self.navigationController?.pushViewController(viewController, animated: animated)
    }
    
    public func pop() {
        self.pop(animated: true)
    }
    
    func pop(animated: Bool) {
        let _ = self.navigationController?.popViewController(animated: animated)
    }
}

// MARK:- Alert methods

extension UIViewController {
    func showAlertWithError(error: NSError) {
        UIAlertController.showErrorAlert(message: error.localizedDescription, controller: self)
    }
    
    func showError(message: String?, isWarning: Bool) {
        if isWarning {
            UIAlertController.showWarningAlert(message: message, controller: self)
        } else {
            UIAlertController.showErrorAlert(message: message, controller: self)
        }
    }
    
    func showAlert(title: String?) {
        UIAlertController.showCustomAlert(title: title, message: nil, controller: self)
    }
    
    func showAlert(title: String?, message: String?) {
        UIAlertController.showCustomAlert(title: title, message: message, controller: self)
    }
    
    func showAlert(title: String?, message: String?, actionBlock: (() -> ())?) {
        UIAlertController.showActionAlert(title: title, message: message, inViewController: self, actionBlock: actionBlock)
    }
    
    func showAlertYesNo(title: String?, message: String?, actionYesBlock: (() -> ())?, actionNoBlock: (() -> ())?) {
        UIAlertController.showActionAlertYesNo(title: title, message: message, inViewController: self, actionYesBlock: actionYesBlock, actionNoBlock: actionNoBlock)
    }
}
