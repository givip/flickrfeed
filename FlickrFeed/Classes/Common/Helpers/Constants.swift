//
//  Constants.swift
//  FlickrFeed
//
//  Created by Givi Pataridze on 08.02.17.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation

struct Keys {
    static let key = "550bacb00fd1e6f882ff696173cf49f1"
    static let secret = "33b8d85cef2b1795"
}

struct URLs {
    static let baseURL = "https://api.flickr.com/services/rest/"
}

struct Parameters {
    static let defaultParams: [String: Any] = ["method"        : "flickr.photos.search",
                                               "api_key"       : Keys.key,
                                               "format"        : "json",
                                               "nojsoncallback": 1,
                                               "safe_search"   : 1]
}

struct Source {
    static let count = 20
}
